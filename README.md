# CURRENCY CONVERSION 

## How to run this app

 1. `git clone https://gitlab.com/veznidav/currency-conversion.git`
 2. `cd currency-conversion`
 3. `cd server`
 4. `yarn install`
 5. `yarn run dev`
 6. `// open another terminal`
 7. `cd client`
 8. `yarn install`
 9. `yarn start`

## Notes

 - As a source currency, only USD is available as I did not find any free 3rd party API for getting rates from
 - Not everything is validated and not all failure cases are handled (but I know about them :-) )
 - I will be happy to explain and comment on all my decisions