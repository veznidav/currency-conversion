import styled from "styled-components";

const Container = styled.div`
  position: relative;
  max-width: 50rem;
  width: 100%;
  margin-left: auto;
  margin-right: auto;
  padding-left: 2rem;
  padding-right: 2rem;
`;

export default Container;
