export type Currency = {
  id: number;
  name: string;
};

export type CurrencyData = {
  currencies: Currency[];
};

export type ConversionResult = {
  conversion: ConversionData;
  statsOverview: StatsOverview;
};

export type ConversionData = {
  success: boolean;
  result?: number;
};

export type StatsOverview = {
  topTargetCurrency: string;
  totalAmountConverted: number;
  totalRequests: number;
};

export type ConversionVars = {
  currency: string;
  targetCurrency: string;
  amount: string;
};
