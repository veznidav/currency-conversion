import React, { useState } from "react";
import styled from "styled-components";
import { useQuery, useLazyQuery } from "@apollo/react-hooks";

import Container from "../commonComponents/Container";
import Stats from "./Stats";
import {
  CurrencyData,
  ConversionResult,
  ConversionVars
} from "./types/conversionTypes";
import { GET_CURRENCIES, GET_CONVERSION } from "./queries/conversionQueries";

const StyledTitle = styled.h1`
  text-align: center;
`;

const FormSection = styled.div`
  margin-top: 2rem;
`;

const StyledInput = styled.input`
  border: 0;
  outline: 0;
  border-bottom: 1px solid #2b564f;
  width: 100%;
`;

const StyledSelect = styled.select`
  border: 0;
  outline: 0;
  border-bottom: 1px solid #2b564f;
  width: 100%;
`;

const StyledLabel = styled.label`
  display: inline-block;
  padding-bottom: 1rem;
`;

const FormRow = styled.div`
  margin-bottom: 2rem;
`;

const StyledButton = styled.button`
  width: 8rem;
  height: 2rem;
`;

const Conversion: React.FC = () => {
  const [amount, setAmount] = useState("0");
  const [currency, setCurrency] = useState("USD");
  const [targetCurrency, setTargetCurrency] = useState("EUR");
  const { data } = useQuery<CurrencyData>(GET_CURRENCIES);

  const [
    getConversion,
    { loading: loadingConversion, data: dataConversion, error: errorConversion }
  ] = useLazyQuery<ConversionResult, ConversionVars>(GET_CONVERSION, {
    fetchPolicy: "network-only"
  });

  const onAmountChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const onlyDigits = /^[0-9\b]+$/;
    if (e.target.value === "" || onlyDigits.test(e.target.value)) {
      setAmount(e.target.value);
    }
  };

  return (
    <Container>
      <StyledTitle>Currency Conversion</StyledTitle>
      <FormSection>
        <FormRow>
          <StyledInput
            type="text"
            placeholder="Amount"
            value={amount}
            pattern="[0-9]*"
            onChange={onAmountChange}
          ></StyledInput>
        </FormRow>
        <FormRow>
          <StyledLabel>Currency</StyledLabel>
          <StyledSelect
            id="currency"
            onChange={(e: React.ChangeEvent<HTMLSelectElement>) =>
              setCurrency(e.target.value)
            }
          >
            {data &&
              data.currencies
                .filter(({ name }) => name === "USD")
                .map(({ id, name }) => {
                  return (
                    <option key={id} value={name}>
                      {name}
                    </option>
                  );
                })}
          </StyledSelect>
        </FormRow>
        <FormRow>
          <StyledLabel>Target currency</StyledLabel>
          <StyledSelect
            id="target-currency"
            onChange={(e: React.ChangeEvent<HTMLSelectElement>) =>
              setTargetCurrency(e.target.value)
            }
            value={targetCurrency}
          >
            {data &&
              data.currencies.map(({ id, name }) => {
                return (
                  <option key={id} value={name}>
                    {name}
                  </option>
                );
              })}
          </StyledSelect>
        </FormRow>
        <FormRow>
          <StyledButton
            onClick={() =>
              getConversion({ variables: { currency, targetCurrency, amount } })
            }
          >
            Convert
          </StyledButton>
        </FormRow>
        <FormRow>
          {loadingConversion
            ? "Getting results..."
            : dataConversion?.conversion
            ? dataConversion.conversion.success
              ? `It's ${dataConversion.conversion.result}`
              : "Cannot get the result"
            : ""}
          {errorConversion && "Server is not available."}
        </FormRow>
      </FormSection>
      {dataConversion?.statsOverview && (
        <Stats data={dataConversion?.statsOverview} />
      )}
    </Container>
  );
};

export default Conversion;
