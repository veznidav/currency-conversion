import gql from "graphql-tag";

export const GET_CURRENCIES = gql`
  query Currencies {
    currencies {
      id
      name
    }
  }
`;

export const GET_CONVERSION = gql`
  query getConversion(
    $currency: String!
    $targetCurrency: String!
    $amount: String!
  ) {
    conversion(
      currency: $currency
      targetCurrency: $targetCurrency
      amount: $amount
    ) {
      success
      result
    }
    statsOverview {
      topTargetCurrency
      totalAmountConverted
      totalRequests
    }
  }
`;
