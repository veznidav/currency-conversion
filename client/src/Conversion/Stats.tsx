import React, { useState } from "react";
import styled from "styled-components";
import { StatsOverview } from "./types/conversionTypes";

const Column = styled.div`
  width: 100%;
`;

type StatsProps = {
  data: StatsOverview;
};

const Stats: React.FC<StatsProps> = ({ data }) => {
  return (
    <>
      <Column>Most popular target currency: {data.topTargetCurrency}</Column>
      <Column>
        Total amount converted (in USD): {data.totalAmountConverted}
      </Column>
      <Column>Total number of conversions: {data.totalRequests}</Column>
    </>
  );
};

export default Stats;
