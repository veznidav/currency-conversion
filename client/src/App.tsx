import React from "react";
import Conversion from "./Conversion";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "@apollo/react-hooks";

const client = new ApolloClient({
  uri: "http://localhost:3001/graphql"
});

const App = () => (
  <ApolloProvider client={client}>
    <Conversion />
  </ApolloProvider>
);
export default App;
