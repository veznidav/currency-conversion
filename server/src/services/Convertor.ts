import { CurrencyLayerAPI } from "./CurrencyLayerAPI";
import {
  ConversionResult,
  ConversionProvider
} from "../types/currencyConversion";

const rateProvider = CurrencyLayerAPI;

export const Convertor: ConversionProvider = {
  convert: async (currency: string, targetCurrency: string, amount: number) => {
    const currentRate = await rateProvider.getRate(currency, targetCurrency);
    if (currentRate.success && currentRate.rate) {
      const result = currentRate.rate * amount;
      return <ConversionResult>{
        success: true,
        result: +result.toFixed(2)
      };
    }
    return <ConversionResult>{ success: false };
  }
};
