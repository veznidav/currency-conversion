import low from "lowdb";
import FileSync from "lowdb/adapters/FileSync";
import {
  TargetCurrencyStats,
  ConversionAmountStats,
  RequestsStats
} from "../types/statsCollector";

const adapter = new FileSync("db.json");
const db = low(adapter);

db.defaults({
  destinationCurrencyStats: [],
  amountConverted: { total: 0 },
  requests: { total: 0 }
}).write();

export const Stats = {
  collectTargetCurrency: (targetCurrency: string) => {
    const targetCurrencyStatsDataTable = db.get("destinationCurrencyStats");
    const targetCurrencyStatsValue = targetCurrencyStatsDataTable
      .find({ name: targetCurrency })
      .value();

    if (targetCurrencyStatsValue) {
      let counter = targetCurrencyStatsValue.counter;

      targetCurrencyStatsDataTable
        .find({ name: targetCurrency })
        .assign({ counter: ++counter })
        .write();
    } else {
      const newTargetCurrency: TargetCurrencyStats = {
        name: targetCurrency,
        counter: 1
      };
      targetCurrencyStatsDataTable.push(newTargetCurrency).write();
    }
  },

  collectTotalAmount: (amount: number) => {
    const currentValue = db.get("amountConverted").value().total;
    db.get("amountConverted")
      .assign(<ConversionAmountStats>{ total: currentValue + amount })
      .write();
  },

  collectTotalRequests: () => {
    const currentValue = db.get("requests").value().total;
    db.get("requests")
      .assign(<RequestsStats>{ total: ++currentValue })
      .write();
  },

  getTopTargetCurrency: (): string => {
    const result = db
      .get("destinationCurrencyStats")
      .sortBy("counter")
      .last()
      .value().name;

    return result;
  },

  getTotalAmountConverted: (): number => {
    const result = db.get("amountConverted").value().total;
    return result;
  },

  getTotalRequests: (): number => {
    const result = db.get("requests").value().total;
    return result;
  }
};
