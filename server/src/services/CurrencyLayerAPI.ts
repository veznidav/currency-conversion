import fetch from "node-fetch";
import { RateProvider, RateResult } from "../types/currencyConversion";

const accessKey = "bb8e3ac1f13895bfb8ff7a9f55170327";
const urlBase = `http://api.currencylayer.com/live?access_key=${accessKey}`;

export const CurrencyLayerAPI: RateProvider = {
  getRate: async (currency: string, targetCurrency: string) => {
    const url = `${urlBase}&source=${currency}&currencies=${targetCurrency}&format=1`;
    try {
      const response = await fetch(url);
      const json = await response.json();
      if (json.success) {
        if (json.quotes && json.quotes[`${currency}${targetCurrency}`]) {
          return <RateResult>{
            success: true,
            rate: parseFloat(json.quotes[`${currency}${targetCurrency}`])
          };
        }
      }
      return <RateResult>{ success: false };
    } catch (error) {
      return <RateResult>{ success: false };
    }
  }
};
