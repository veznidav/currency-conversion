export type RateProvider = {
  getRate: (currency: string, targetCurrency: string) => Promise<RateResult>;
};

export type RateResult = {
  success: boolean;
  rate?: number;
};

export type ConversionResult = {
  success: boolean;
  result?: number;
};

export type ConversionProvider = {
  convert: (
    currency: string,
    targetCurrency: string,
    amount: number
  ) => Promise<ConversionResult>;
};

export type Currency = {
  id: number;
  name: string;
};

export type ConversionArgs = {
  currency: string;
  targetCurrency: string;
  amount: string;
};
