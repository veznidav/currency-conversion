export type TargetCurrencyStats = {
  name: string;
  counter: number;
};

export type ConversionAmountStats = {
  total: number;
};

export type RequestsStats = {
  total: number;
};

export type StatsOverview = {
  topTargetCurrency: string;
  totalAmountConverted: number;
  totalRequests: number;
};
