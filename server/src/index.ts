import express = require("express");
import { ApolloServer } from "apollo-server-express";
import cors from "cors";
import { createServer } from "http";
import schema from "./schema";

const app: express.Application = express();
const server: ApolloServer = new ApolloServer({
  schema
});

app.use("*", cors());

server.applyMiddleware({
  app,
  path: "/graphql"
});

const httpServer = createServer(app);

httpServer.listen({ port: 3001 }, (): void =>
  console.log(`\n GraphQL is now running on http://localhost:3001/graphql`)
);
