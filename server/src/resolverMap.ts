import { IResolvers } from "graphql-tools";
import { Convertor } from "./services/Convertor";
import { Stats } from "./services/Stats";
import {
  ConversionResult,
  Currency,
  ConversionArgs
} from "./types/currencyConversion";
import { StatsOverview } from "./types/statsCollector";

export const currencies: Currency[] = [
  { id: 1, name: "EUR" },
  { id: 2, name: "GBP" },
  { id: 3, name: "USD" }
];

const resolverMap: IResolvers = {
  Query: {
    currencies(): Currency[] {
      return currencies;
    },
    async conversion(
      parent,
      args: ConversionArgs,
      context,
      info
    ): Promise<ConversionResult> {
      const { currency, targetCurrency, amount } = args;
      Stats.collectTargetCurrency(targetCurrency);
      Stats.collectTotalAmount(parseFloat(amount));
      Stats.collectTotalRequests();
      const result = await Convertor.convert(
        currency,
        targetCurrency,
        parseFloat(amount)
      );
      if (result.success && result.result) {
        return <ConversionResult>{ success: true, result: result.result };
      }
      return <ConversionResult>{ success: false };
    },
    statsOverview(): StatsOverview {
      return <StatsOverview>{
        topTargetCurrency: Stats.getTopTargetCurrency(),
        totalAmountConverted: Stats.getTotalAmountConverted(),
        totalRequests: Stats.getTotalRequests()
      };
    }
  }
};
export default resolverMap;
